package com.austinkregel.java.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Austin Kregel
 */
public class Utils {

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(String s) {
        System.out.println(s);
    }

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(int s) {
        System.out.println(s);
    }

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(char s) {
        System.out.println(s);
    }

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(boolean s) {
        System.out.println(s);
    }

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(long s) {
        System.out.println(s);
    }

    /**
     * Prints things...
     *
     * @param s
     * @return
     */
    public void print(float s) {
        System.out.println(s);
    }

    /**
     * Gets the factorial of an int.
     *
     * @param n
     * @return
     */
    public long factorial(int n) {
        if (n <= 1)
            return n;
        return n * factorial(n - 1);
    }

    /**
     * Gets the factorial of an float.
     *
     * @param n
     * @return
     */
    public float factorial(float n) {
        if (n <= 1)
            return n;
        return n * factorial(n - 1);
    }

    /**
     * Gets the factorial of an bool :)
     *
     * @param n
     * @return
     */
    public long factorial(long n) {
        if (n <= 1)
            return n;
        return n * factorial(n - 1);
    }

    /**
     * Gets the factorial of an bool :)
     *
     * @param n
     * @return
     */
    public int factorial(boolean n) {
        return 1;
    }

    /**
     * Compares two strings to see if they are the same...
     *
     * @param str
     * @param searching_for
     * @return
     */
    public boolean str_compare(String str, String searching_for) {
        if (this.is_null(str) && this.is_null(searching_for))
            return true;
        else if (this.is_null(str) | this.is_null(searching_for))
            return false;
        return str.equalsIgnoreCase(searching_for);
    }

    /**
     * See if a string contains another string.
     *
     * @param needle
     * @param haystack
     * @return
     */
    public boolean str_contains(String needle, String haystack) {
        if (this.is_null(needle) && this.is_null(haystack))
            return true;
        else if (this.is_null(needle) | this.is_null(haystack))
            return false;
        return haystack.contains(needle);
        ;
    }

    /**
     * Checks if a string is null
     *
     * @param a
     * @return
     */
    public boolean is_null(String a) {
        return a == null | a == "" | a.isEmpty();
    }

    /**
     * Checks if an int is null
     *
     * @param a
     * @return
     */
    public boolean is_null(int a) {
        return a == 0;
    }

    /**
     * Checks if a long is null
     *
     * @param a
     * @return
     */
    public boolean is_null(long a) {
        return a == 0;
    }

    /**
     * Checks if a string is float
     *
     * @param a
     * @return
     */
    public boolean is_null(float a) {
        return a == 0;
    }

    /**
     * Checks if a char is null
     *
     * @param a
     * @return boolean
     */
    public boolean is_null(char a) {
        return a == '\0' | a == ' ';
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(int value, int total) {
        return ((float) value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(float value, int total) {
        return (value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(int value, float total) {
        return ((float) value / total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(float value, float total) {
        return (value / total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(long value, long total) {
        return ((float) value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(int value, long total) {
        return ((float) value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(long value, int total) {
        return ((float) value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return
     */
    public float percentage(float value, long total) {
        return (value / (float) total) * 100F;
    }

    /**
     * calculate a percentage
     *
     * @param value
     * @param total
     * @return float
     */
    public float percentage(long value, float total) {
        return ((float) value / total) * 100F;
    }

    /**
     * Get a string
     *
     * @return String
     */
    public String getString() {
        Scanner scanner = null;
        String input;
        try {
            scanner = new Scanner(System.in);
            //rest of the code
            input = scanner.next();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return input;
    }

    /**
     * Get an int
     *
     * @return int
     */
    public int getInt() {
        Scanner scanner = null;
        int input;
        try {
            scanner = new Scanner(System.in);
            input = scanner.nextInt();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return input;
    }

    /**
     * Get a double
     *
     * @return double
     */
    public double getDouble() {
        Scanner scanner = null;
        double input;
        try {
            scanner = new Scanner(System.in);
            input = scanner.next();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return input;
    }

    /**
     * Get a char
     *
     * @return char
     */
    public char getChar() {
        Scanner scanner = null;
        char input;
        try {
            scanner = new Scanner(System.in);
            input = scanner.next();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return input;
    }

    /**
     * Get a float
     *
     * @return
     */
    public double getFloat() {
        Scanner scanner = null;
        float input;
        try {
            scanner = new Scanner(System.in);
            input = scanner.next();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return input;
    }
    /**
     * This will (not) convert Scientific notation to a number.
     */
    public float sciFiNotation(String expression){
        String[] parse = expression.split('e');
        String answer;
        int exponent = Integer.parseInt(parse[1]);

        if(exponent < 0){
            answer = "0.";
            parse[0].replace(".", "");
            String p = Math.pow(10, exponent-1).toString();
            return Float.parseFloat(answer + p+parse[0]);
        } else {
            String[] afterPeriod = parse[0].split(".");
            if(afterPeriod.length > 1){
                char[] sigfigs = afterPeriod[1].toCharArray();
                if(sigfigs.length < exponent){

                }

            }

        }
    }
    /**
     * This will look, and read a file if it is found, if it's not found,
     * this will print an exception
     */
    public void fileInput(String filepath){
        Scanner inFile = new Scanner(new FileReader(filepath));
        try {
            inFile.
        } catch(IOException e){
            this.print(e.getMessage());
        } finally {
            if(inFile != null)
                inFile.close();
        }
    }
    /**
     * This will just close the program.
     */
    public void quit(){
        System.exit();
    }
    /**
     * This will open a JOptionPane confirming if you want to close this program.
     */
    public void quitPrompt(){
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure you wish to quit this program?", "You are about to quit this program", dialogButton);
        if(dialogResult == 0) {
            this.print("Quiting program!");
            this.quit();
        } else {
            this.print("Resuming program!");
        }
    }
    /**
     * This will get an input string, remove everything that isn't a decimal or a number,
     * convert the string to a double then it will round it up. Now we desire it to do
     * round it because if it truncates it it is essentially rounding down. To avoid
     * a possible zero value, we shall round up and hope for the best.
     *
     * @return int
     */
    public int getInt() {
        int number = (int) Math.ceil(Double.parseDouble(scanner.nextLine().replaceAll("[^\\d.-]", "")));
        // We don't want a negative number.
        if (number < 0)
            return 0;
        return number;
    }
}
