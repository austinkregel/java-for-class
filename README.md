### What is this repository for? ###

* Bootstrapping your Java projects with basic functions.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Extract the `src/` and merge it with your existing `src/` folder.
* Thus far, you can use the functions below
```java
Utils.print(String);
Utils.print(int);
Utils.print(boolean);
Utils.print(long);
Utils.print(float);

Utils.factorial(int);
Utils.factorial(long);
Utils.factorial(float);
Utils.factorials(boolean);

Utils.str_compare(String needle, String haystack); // If they are the same
Utils.str_contains(String needle, String haystack);// If one has the other

Utils.is_null(String);
Utils.is_null(int);
Utils.is_null(long);
Utils.is_null(float);
Utils.is_null(char);

Utils.percentage(int, int);
Utils.percentage(int, float);
Utils.percentage(float, int);
Utils.percentage(float, float);
Utils.percentage(... , ...); // You should get the idea... (any number)

```
### Contribution guidelines ###

* Format your code to use PSR-2 standards (note my code will not totally be formatted to the PSR-2 standards)
* Make an issue first noting your changes, then make a pull request with those changes. (it can be editing my files or adding your own)

### Who do I talk to? ###
* [Austin Kregel](mailto:me@austinkregel.com) (The main admin)
* Or you can just make an issues request.